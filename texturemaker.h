// to run on mac: use OpenGl/glu.h and GLUT/glut.h
#ifdef __APPLE__
#include <OpenGl/glu.h>
#include <GLUT/glut.h>
#else
// to run on Dec10: use GL/glu.h and GL/glut.h
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <iostream>

#define CUBE_RES 64.0


GLuint tex3D(float * data, int resolution, GLint format) {
    glEnable(GL_TEXTURE_3D);

    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_3D, tex);
    // set out-of-bounds behaviour
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    // set scaling behavior
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // set texture to an image texture 
    //           gl_texture_3d, LoD, internal format, dimentions ->               , 0,  format, datatype, data
    glTexImage3D(GL_TEXTURE_3D,   0,         format, resolution, resolution, resolution, 0, format, GL_FLOAT, data);
    // bind to texture
    glBindTexture(GL_TEXTURE_3D, tex);
    return tex;
}