// to run on mac: use OpenGl/glu.h and GLUT/glut.h
#ifdef __APPLE__
#include <OpenGl/glu.h>
#include <GLUT/glut.h>
#else
// to run on Dec10: use GL/glu.h and GL/glut.h
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <QApplication>
#include <QVBoxLayout>
#include "ProjectWindow.h"

int main(int argc, char *argv[]) {
	// init glut
	glutInit(&argc, argv);
	// create the application
	QApplication app(argc, argv);

	// create a master widget
    ProjectWindow *window = new ProjectWindow(NULL);

	// resize the window
	window->resize(890, 625);

	// show the label
	window->show();

	// start it running
	app.exec();

	// clean up
	//	delete controller;
	delete window;
	
	// return to caller
	return 0; 
} // main()
