#include <iostream>
#include <cmath>

// Perlin's hash value array
// Perlin's 2002 code uses a literal array of size 256
// Perlin doubles this array into an array of 512 using some code
// I do the same, only I double it with copy and paste
const int p[512] = {151,160,137,91,90,15,
   131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
   190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
   88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
   77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
   102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
   135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
   5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
   223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
   129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
   251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
   49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
   138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180, 
   151,160,137,91,90,15,
   131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
   190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
   88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
   77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
   102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
   135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
   5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
   223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
   129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
   251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
   49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
   138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};

// Schneider's 2016 remap function
float remap(float val, float old_min, float old_max, float new_min, float new_max) {
    return new_min + (((val - old_min) / (old_max - old_min)) * (new_max - new_min));
}

// t should be a value between 0 and 1
// returns a value that's (t*100)% of the way between a and b
float mix (float t, float a, float b) {
    return a + (t * (b - a));
}

// returns 6t^5 - 15t^4 + 10t^3
float quintic_fade(float t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

int perlin_hash(int x, int y, int z, int frequency) {
    x %= frequency, y %= frequency, z %= frequency;

    return p[p[p[x] + y] + z];
}

// Perlin has a fancy way of doing this, but the switch statement is faster and more legible
// dot product of location vector and a random vector
// uses one of the following 12 vectors:
// (1,1,0),(-1,1,0),(1,-1,0),(-1,-1,0),
// (1,0,1),(-1,0,1),(1,0,-1),(-1,0,-1),
// (0,1,1),(0,-1,1),(0,1,-1),(0,-1,-1)
// 4 vectors will be represented twice, bringing the number up to 16
// this allows us to use a bitwise AND operation rather than a division by 12
// code source: http://riven8192.blogspot.com/2010/08/calculate-perlinnoise-twice-as-fast.html
float grad(int hash, float x, float y, float z) {
    switch (hash & 0xF) { // mod 16 but using bits, & 0xF is faster than % 16
    case 0x0: return  x + y;
    case 0x1: return -x + y;
    case 0x2: return  x - y;
    case 0x3: return -x - y;
    case 0x4: return  x + z;
    case 0x5: return -x + z;
    case 0x6: return  x - z;
    case 0x7: return -x - z;
    case 0x8: return  y + z;
    case 0x9: return -y + z;
    case 0xA: return  y - z;
    case 0xB: return -y - z;
    // redundant vectors:
    case 0xC: return  x + y;
    case 0xD: return -x + y;
    case 0xE: return -y + z;
    case 0xF: return -y - z;
    default: break; // won't happen
    }
    return 0.0f; // will never be reached
}


float perlin_noise_at(float x, float y, float z, int frequency) {
    // 1/256 = 0.00390625
    //x *= 0.00390625, y *= 0.00390625, z *= 0.00390625;
    // 1/128
    //x *= 0.0078125, y *= 0.0078125, z *= 0.0078125;
    // 1/64
    x *= 0.015625, y *= 0.015625, z *= 0.015625;
    // 1/32
    //x *= 0.03125, y *= 0.03125, z *= 0.03125;
    x *= frequency, y *= frequency, z *= frequency;

    // ID values- floored indices of the unit cube in space
    int idx = ((int)x) & 255;
    int idy = ((int)y) & 255;
    int idz = ((int)z) & 255;
    // Location values- position of point within the unit cube
    float locx = x - (int)x;
    float locy = y - (int)y;
    float locz = z - (int)z;

    // compute faded location
    float u = quintic_fade(locx);
    float v = quintic_fade(locy);
    float w = quintic_fade(locz);

    // hash values
    int aaa = perlin_hash(idx    , idy    , idz    , frequency);
    int aab = perlin_hash(idx    , idy    , idz + 1, frequency);
    int aba = perlin_hash(idx    , idy + 1, idz    , frequency);
    int baa = perlin_hash(idx + 1, idy    , idz    , frequency);
    int abb = perlin_hash(idx    , idy + 1, idz + 1, frequency);
    int bba = perlin_hash(idx + 1, idy + 1, idz    , frequency);
    int bab = perlin_hash(idx + 1, idy    , idz + 1, frequency);
    int bbb = perlin_hash(idx + 1, idy + 1, idz + 1, frequency);

    // interpolate
    float x1, x2, y1, y2;
    // first set of 4 planar points
    x1 = mix(u, grad(aaa, locx, locy, locz), grad(baa, locx - 1.0f, locy, locz));
    x2 = mix(u, grad(aba, locx, locy - 1.0f, locz), grad(bba, locx - 1.0f, locy - 1.0f, locz));
    y1 = mix(v, x1, x2);
    // second 4 points
    x1 = mix(u, grad(aab, locx, locy, locz - 1.0f), grad(bab, locx - 1.0f, locy, locz - 1.0f));
    x2 = mix(u, grad(abb, locx, locy - 1.0f, locz - 1.0f), grad(bbb, locx - 1.0f, locy - 1.0f, locz - 1.0f));
    y2 = mix(v, x1, x2);
    //mix top and bottom
    return mix(w, y1, y2);
}


float perlin_fbm(float x, float y, float z, float frequency) {
    return abs( perlin_noise_at(x, y, z, 1 * frequency) * .65 
        +  perlin_noise_at(x, y, z, 2 * frequency) * .234
        +  perlin_noise_at(x, y, z, 4 * frequency) * .123
        +  perlin_noise_at(x, y, z, 8 * frequency) * .04444
        +  perlin_noise_at(x, y, z, 16 * frequency) * .02435 );
}

void worley_hash(int x, int y, int z, int frequency, float * out) {
    // avoid -1 case
    x++, y++, z++;
    // handles wrapping
    x = x % frequency; 
    y = y % frequency;
    z = z % frequency;

    // 1/255 = 0.003921568627
    out[0] = (float)p[p[p[p[x] + y] + z] + x] * 0.003921568627f;
    out[1] = (float)p[p[p[p[x] + y] + z] + y] * 0.003921568627f;
    out[2] = (float)p[p[p[p[x] + y] + z] + z] * 0.003921568627f;
}

float length3fv(float * vec) {
    return sqrt((vec[0] * vec[0]) + (vec[1] * vec[1]) + (vec[2] * vec[2]));
}

float worley_at(float x, float y, float z, int frequency) {
    // 1/256 = 0.00390625
    //x *= 0.00390625, y *= 0.00390625, z *= 0.00390625;
    // 1/128
    //x *= 0.0078125, y *= 0.0078125, z *= 0.0078125;
    // 1/64
    x *= 0.015625, y *= 0.015625, z *= 0.015625;
    // 1/32
    //x *= 0.03125, y *= 0.03125, z *= 0.03125;
    x *= frequency, y *= frequency, z *= frequency;
    // ID values- floored indices of the unit cube in space
    int idx = (int)x & 255;
    int idy = (int)y & 255;
    int idz = (int)z & 255;
    // Location values- position of point within the unit cube
    float locx = x - (int)x;
    float locy = y - (int)y;
    float locz = z - (int)z;

    float Point_of_Interest[3];
    float minDist = 1000.0f;
    float newDist;
    for (int i = -1; i <= 1; i ++) {
        for (int j = -1; j <= 1; j++) {
            for (int k = -1; k <= 1; k++) {
                worley_hash(idx + i, idy + j, idz + k, frequency, Point_of_Interest);
                Point_of_Interest[0] += i - locx;
                Point_of_Interest[1] += j - locy; 
                Point_of_Interest[2] += k - locz;
                newDist = length3fv(Point_of_Interest);
                if (newDist < minDist) minDist = newDist;
            }
        }
    }
    return 1.0f - minDist;
}


float worley_fbm(float x, float y, float z, int frequency) {
    return worley_at(x, y, z, frequency) * 0.625 
        +  worley_at(x * 2.0f, y * 2.0f, z * 2.0f, frequency * 2) * 0.25
        +  worley_at(x * 4.0f, y * 4.0f, z * 4.0f, frequency * 4) * 0.125;
}

// as described by Schneider (2016)
float perlin_worley_fbm(float x, float y, float z) {
    float w = worley_fbm(x, y, z, 4);
    float p = perlin_fbm(x, y, z, 4);
    return remap(p, 0.0, 1.0, w, 1.0);
}

float cloud_fbm(float x, float y, float z) {
    float pw = perlin_worley_fbm(x, y, z);
    float w = worley_fbm(x, y, z, 12) * 0.625
            + worley_fbm(x, y, z, 27) * 0.27
            + worley_fbm(x, y, z, 49) * 0.1234;
    return remap(pw, w - 1.0, 1.0, 0.0, 1.0);
}

float cloud_fbm_w_coverage(float x, float y, float z, float clearsky) {
    return remap(cloud_fbm(x, y, z), clearsky, 1.0, 0.0, 1.0);
}

void cone_sample_vector(int t, int frequency, float * out) {
    worley_hash(t + 23, t + 13, t + 59, frequency, out);
    remap(out[0], 0.0, 1.0, -1.0, 1.0);
    remap(out[1], 0.0, 1.0, -1.0, 1.0);
    remap(out[2], 0.0, 1.0, -1.0, 1.0);
}