uniform vec2 iResolution;
uniform float iTime;


uniform vec4 bgCol;

uniform float dist;
uniform vec3 camera;
uniform vec3 origin_to_target;
uniform vec3 left;
uniform vec3 up;

uniform vec3 sun_dir;
uniform float phase;
uniform float clearsky;
uniform float opac;
uniform float lopac;
uniform float absorb;
uniform float freqmod;

uniform vec3 coneVectors[8];

uniform vec3 cloudColor;
uniform vec3 sunColor;

uniform sampler3D tex;


#define ray_it 200
#define light_it 8 // aka length of coneVectors
float inv_ray_it = 1. / float(ray_it);


float opacity = 65.;
vec3 cloudCol = vec3(0.8, 0.8, 0.8);

float lightOpacity = 35.;
vec3 lightCol = vec3(.9, .7, .1);

float absorption = 1.;
float fm = 1.;

// Schneider remap function
float remap(float val, float old_min, float old_max, float new_min, float new_max) {
    return new_min + (((val - old_min) / (old_max - old_min)) * (new_max - new_min));
}

// makes a sphere
// x - (length(p) * .05) is inverse distance function 
// points with length less than 20x get positive values, outside of x radius values become negative
// high enough values from the FBM can outweigh the negative distance values
float scene(vec3 p) {
    //if ((1.5 - length(p) * .05) < 0.) return 0.;
    return .5 - (length(p) * .05) + texture3D(tex, p * 0.017 * fm + vec3(iTime * 0.15, 0., 0.)).x;
}

// same as above except w animation and coverage using the remap function
float scene_w_coverage(vec3 p) {
    return .5 - (length(p) * .05) + remap(texture3D(tex, p * 0.027 * fm + vec3(iTime * 0.15, 0., 0.)).x, clearsky, 1.0, 0.0, 1.0);
}


// using Henyey-Greenstein phase function to calculate probabilty of forward scattering
//                   1           1 - g^2
// pHG(theta, g) = ---- ----------------------------
//                 4 PI    1 + g^2 - 2g cos(theta)^1.5
// where theta is the angle between the viewing direction and the sun direction
// and g is an eccentricity
float HG_probability(vec3 view, float eccentricity) {
    float cos_theta = dot(sun_dir, view);
    return (4. * 3.1416 * (1. - eccentricity * eccentricity)) /
           (1. + eccentricity * eccentricity - (2. * eccentricity * pow(cos_theta, 1.5)));
}

// powdered sugar effect, described by Schneider, is what gives clouds some dark edges even when directly facing light source
float powder_effect(float lightDensity) {
    return 1. - exp( -2. * lightDensity);
}

void main( void ) {
    // Normalized pixel coordinates so that 0,0 is the center
    vec2 uv = (2. * gl_FragCoord.xy - iResolution.xy)/iResolution.y;


    /// raycasting 
    // rays start at origin
    vec3 origin = dist * camera.yxz;
    // normal from origin to pixel
    vec3 dir = normalize(uv.x * left    // move left based on pixel position x
                       + uv.y * up      // move up based on pixel position y
                       + 1.5 * origin_to_target); // incorporate origin point
    
    // ray loop things
    float zTravel = 60.;
    float zStep = zTravel * inv_ray_it;
    vec3 rayPoint = origin;
    vec3 color = vec3(0.);
    float density = 0.;
    //Transmittence
    float T = 1.;

    // light loop things
    float lightSample;
    float lzTravel = zTravel * 0.15;
    float lStep = lzTravel * 0.125; // lzTravel / 8 steps
    vec3 light_step = sun_dir * lStep;
    float cone_spread_multiplier = length(light_step);
    
    // skip where there won't be values
    rayPoint += dir * (dist - (zTravel * 0.5));


    float l_density = 0.;
    float lightEnergy = 0.;

    if (opac != 0.) opacity = opac;
    if (lopac != 0.) lightOpacity = lopac;
    if (absorb != 0. ) absorption = absorb;
    if (freqmod != 0. ) fm = freqmod;

    // ray loop
    for(int i = 0; i < ray_it; i ++) {
        density = scene_w_coverage(rayPoint);
        if (density > 0.) {
            
            density *= inv_ray_it;
            T *= 1. - (density * absorption * 100.); 

            // allows transmittence to be "used up" as it goes through the cloud
            if (T <= .01) break;

            // light sampling
            // copy point for use in sampling loop
            vec3 samplePoint = rayPoint;
            for (int j = 0; j < 8; j++) {
                samplePoint += light_step
                    + (cone_spread_multiplier * float(j) * coneVectors[j]);
                
                lightSample = scene_w_coverage(samplePoint);
                if (lightSample > 0.) {
                    l_density += lightSample;
                }
            }

            lightEnergy = 0.2 * exp(-1. * l_density) * powder_effect(l_density) * HG_probability(dir, 0.2);
            color += (cloudCol * opacity + lightCol * lightEnergy * lightOpacity) * density * T; 
        }
        rayPoint += dir * zStep;
    }

    // Output to screen
    gl_FragColor = vec4(color, 1.0) + bgCol;
}
