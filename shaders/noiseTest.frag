uniform vec2 iResolution;
uniform float iTime;

vec2 poi_gen (vec2 gridIndex) {
    // random val calculated based on gridindex and time
    // ensures for the same grid index and time any calculated points are identical
    // adding 1 to gridIndex prevents multiplying by 0 issues
    return fract(sin(vec2(dot(gridIndex + 1.0, vec2(127.4, 723.8)), 
                dot(gridIndex + 1.0, vec2(837.7, 208.7)))) * 25787.33);
}

/// Inverse Worley Noise 
float worley(vec2 p, float freq) {
    // assume p is normalized
    // split p into id and position
    vec2 id = floor(p);
    vec2 pos = fract(p);
    
    float minDist = 100.0; // needs to be too big initially
    for(int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
            vec2 offset = vec2(x, y);
            vec2 poi = poi_gen(mod(id + offset, vec2(freq))); // use mod for tiling
            poi += offset;
            vec2 diff = poi - pos;
            minDist = min(minDist, length(diff));
        }
    }
    return 1.0 - minDist; // inverting noise
}

/// Compound Worley Noise
// uses the Worley FBM described by Schneider's chapter in GPU Pro 7
float worleyFBM(vec2 p, float freq) {
    return worley(p * freq, freq) * 0.625 +
           worley(p * freq * 2., freq * 2.) * 0.25 +
           worley(p * freq * 4., freq * 4.) * 0.125;
}

/// PERLIN
// modified to tile from thebookofshaders.com
float perlin(vec2 uv, float freq) {
    vec2 p = uv * freq;
    // split into ID and position
    vec2 id = floor(p);
    vec2 pos = fract(p);
    
    // tile corners
    float a = poi_gen(id).x;
    float b = poi_gen(mod(id + vec2(1., 0.), freq)).x;
    float c = poi_gen(mod(id + vec2(0., 1.), freq)).x;
    float d = poi_gen(mod(id + vec2(1., 1.), freq)).x;
    
    //smooth interpolation
    vec2 u = smoothstep(0., 1., pos);
    //u = pos * pos * (3. - 2. * pos);
    
    // mix 4 corners using position as percentage
    return mix(a, b, u.x) +
           (c - a) * u.y * (1. - u.x) +
           (d - b) * u.x * u.y;
}


/// Multi-octave perlin noise compounder
float perlinFBM(vec2 p, float freq, int octaves) {
    float noise = 0.;
    float w = 0.5;
    float c = 1.;
    for (int i = 0; i < octaves; ++i) {
        noise += w * perlin(p , freq * c);
        c = c * 2.;
        w *= 0.5;
    }

    return noise;
}


/// PerlinWorley
// remapping as described by Schneider's chapter in GPU Pro 7
float remap(float val, float old_min, float old_max, float new_min, float new_max) {
    return new_min + (((val - old_min) / (old_max - old_min)) * (new_max - new_min));
}

float perlin_worley(vec2 uv, float freq) {
    float w = worleyFBM(uv, freq);
    float p = perlinFBM(uv, freq, 5);
    p = abs(p * 2. - 1.);
    return remap(p, 0., 1., w, 1.);
}

///cloud
float cloud(vec2 uv, float freq) {
    float pw = perlin_worley(uv, freq);
    float wg = worleyFBM(uv, freq);
    float wb = worleyFBM(uv, freq * 2.);
    float wa = worleyFBM(uv, freq * 4.);
    
    float wfbm = wg * .625 + wb * .25 + wa * 0.125;
    
    return remap(pw, wfbm - 1., 1., 0., 1.);
}

// image will be comprised of t x t tiles
float t = 2.0; 
// noises start generation with freq x freq cells
float freq = 4.0;

void main (void) {
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_FragCoord.xy/iResolution.y;
    
    // create tiles
    //uv *= t;
    // add time animation
    uv -= 0.05 * iTime;
    uv = fract(uv);
    
    vec3 col = vec3(0.0);
  /// => comment/uncomment the below lines to see the contribution of each
    //col += worley(uv * freq, freq);
    //col += worleyFBM(uv, freq);
    //col = vec3(perlin(uv, freq));
    //col = vec3(perlinFBM(uv, freq, 7));
    //col = abs(col * 2. - 1.); // "folding" values down the middle
    //col = vec3(perlin_worley(uv, freq));
    
    
    if (gl_FragCoord.x > iResolution.x / 2.) {
        // remap here acts as a rudimentary "cloud cover" texture described by Schneider in GPU Pro 7
        col = vec3(remap(cloud(uv, freq), 0.75, 1., 0., 1.));
    } else {
        col = vec3(cloud(uv, freq));
    }
    
    
    // Output color
    gl_FragColor = vec4(col,1.0);
}