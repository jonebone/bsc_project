uniform vec2 iResolution;
uniform float iTime;


uniform vec4 bgCol;

uniform float dist;
uniform vec3 camera;
uniform vec3 origin_to_target;
uniform vec3 left;
uniform vec3 up;

uniform vec3 sun_dir;
uniform float lopac;

uniform vec3 coneVectors[8];


uniform sampler3D tex;


#define ray_it 90
#define light_it 8 // aka length of coneVectors
float inv_ray_it = 1. / float(ray_it);


float opacity = 35.;
vec3 cloudCol = vec3(0.3, 0.3, 0.7);

float light_opacity = 10.;
vec3 lightCol = vec3(.9, .1, .1);


// makes a sphere
// x - (length(p) * .05) is inverse distance function 
// points with length less than 20x get positive values, outside of x radius values become negative
// high enough values from the FBM can outweigh the negative distance values
float scene(vec3 p) {
    if ((1.5 - length(p) * .05) < 0.) return 0.;
    return .5 - (length(p) * .05) + texture3D(tex, p * 0.01).x;
}

// should be able to represent optical depth as length(p) * 0.05

void main( void ) {
    // Normalized pixel coordinates so that 0,0 is the center
    vec2 uv = (2. * gl_FragCoord.xy - iResolution.xy)/iResolution.y;

    /// raycasting 
    // rays start at origin
    vec3 origin = dist * camera.yxz;
    // normal from origin to pixel
    vec3 dir = normalize(uv.x * left + uv.y * up + 1.5 * origin_to_target);
    
    // ray loop things
    float zMax = sqrt(900. - (uv.x * uv.x + uv.y * uv.y) );
    float zTravel = 2. * zMax;
    float zStep = zTravel / float(ray_it);
    vec3 rayPoint = origin;
    vec3 color = vec3(0.);
    float density = 0.;
    //Transmittence
    float T = 1.;

    // light loop things
    float lightSample;
    float lzMax = zTravel * 0.5;
    float lStep = lzMax * 0.125;
    vec3 light_step = sun_dir * lStep;
    float cone_spread_multiplier = length(light_step);
    
    rayPoint += dir * (2. * zMax - origin.z);

    // ray loop
    for(int i = 0; i < ray_it; i ++) {
        density = scene(rayPoint);
        
        if (density > 0.) {
            density = density * inv_ray_it;
            T *= 1. - density * 100.; 
            // allows transmittence to be "used up" as it goes through the cloud
            if (T <= .01) break;

            // light sampling
            float lT = 1.;
            // copy point for use in sampling loop
            vec3 samplePoint = rayPoint;
            for (int j = 0; j < 8; j++) {
                lightSample = scene(samplePoint);
                if (lightSample > 0.) {
                    lT *= 1. - (lightSample * 0.125) * 100.;
                    if (lT <= .01) break;
                }
                samplePoint += light_step
                    + (cone_spread_multiplier * float(j) * coneVectors[j]);
            }
            
            color += (cloudCol * opacity + lightCol * light_opacity * lT) * density * T;
            
        }
        rayPoint += dir * zStep;
    }

    // Output to screen
    gl_FragColor = vec4(color,1.0) + bgCol;
}
