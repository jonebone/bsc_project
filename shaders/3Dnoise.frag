uniform vec2 iResolution;
uniform float iTime;

//// *** noisemaking in here *** ////

// remapping as described by Schneider's chapter in GPU Pro 7
float remap(float val, float old_min, float old_max, float new_min, float new_max) {
    return new_min + (((val - old_min) / (old_max - old_min)) * (new_max - new_min));
}

/// point of interest generator
// point of interest is a point we calculate distance to for worley noise
vec3 poi_gen (vec3 gridIndex) {
    // random val calculated based on gridindex and time
    // ensures for the same grid index and time any calculated points are identical
    // adding 1 to gridIndex prevents multiplying by 0 issues
    // NB: side effect of fract is that all values will be positive 
    return fract(sin(vec3(
                dot(gridIndex + 1.0, vec3(127.4, 723.8, 872.4)),
                dot(gridIndex + 1.0, vec3(387.4, 275.4, 567.6)),
                dot(gridIndex + 1.0, vec3(837.7, 208.7, 576.4)) )) * 25787.33);
}

/// Inverse Worley Noise 
float worley(vec3 p, float freq) {
    // split p into id and position
    vec3 id = floor(p);
    vec3 pos = fract(p);
    
    float minDist = 1000.0; // needs to be too big initially
    for(int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
            for (int z = -1; z <= 1; z++) {
                vec3 offset = vec3(x, y, z);
                vec3 poi = poi_gen(mod(id + offset, vec3(freq))); // use mod for tiling
                poi += offset;
                vec3 diff = poi - pos;
                minDist = min(minDist, length(diff));
            }
        }
    }
    return 1.0 - minDist; // inverting noise
}

/// Compound Worley Noise
// uses the Worley FBM described by Schneider's chapter in GPU Pro 7
float worleyFBM(vec3 p, float freq) {
    return worley(p * freq, freq) * 0.625 +
           worley(p * freq * 2., freq * 2.) * 0.25 +
           worley(p * freq * 4., freq * 4.) * 0.125;
}

//// PERLIN
// Perlin noise is a varient of gradient noise
// gradient noise works by interpolating between random values at cell corners
// Perlin noise does the same interpolation of values
// but the Perlin values are found by taking the dot product of 
// a normal vector in a random direction from a cell corner
// and the vector from that cell corner to the point in question
// for more check out http://physbam.stanford.edu/cs448x/old/Procedural_Noise(2f)Categories.html

/// Perlin Hash
vec3 hash(vec3 p) {
    vec3 poi = poi_gen(p);
    // my poi_gen function makes values between 0 and 1
    // this limits the directionality if we used poi_gen alone to get vectors
    // below is a simplified remap from 0...1 to -1...1 
    poi = vec3(-1.) + (poi * 2.);
    return normalize(poi);
}

/// Perlin noise generator 
// get vectors using perlin hash
// dot vectors by (offset - pos)
// interpolate values
float perlin(vec3 uvw, float freq) {
    vec3 p = uvw * freq;
    // split into ID and position
    vec3 id = floor(p);
    vec3 pos = fract(p);
    
    // dotted vals
    float a0 = dot(hash(mod(id + vec3(0., 0., 0.), freq)), pos - vec3(0., 0., 0.));
    float b0 = dot(hash(mod(id + vec3(1., 0., 0.), freq)), pos - vec3(1., 0., 0.));
    float c0 = dot(hash(mod(id + vec3(0., 1., 0.), freq)), pos - vec3(0., 1., 0.));
    float d0 = dot(hash(mod(id + vec3(1., 1., 0.), freq)), pos - vec3(1., 1., 0.));
    
    float a1 = dot(hash(mod(id + vec3(0., 0., 1.), freq)), pos - vec3(0., 0., 1.));
    float b1 = dot(hash(mod(id + vec3(1., 0., 1.), freq)), pos - vec3(1., 0., 1.));
    float c1 = dot(hash(mod(id + vec3(0., 1., 1.), freq)), pos - vec3(0., 1., 1.));
    float d1 = dot(hash(mod(id + vec3(1., 1., 1.), freq)), pos - vec3(1., 1., 1.));
    
    //smooth interpolation
    //vec3 u = smoothstep(0., 1., pos);
    
    // quintic interpolation (6pos^5 - 15pos^4 + 10pos^3)
    vec3 u = pos * pos * pos * (pos *(pos * 6.0 - 15.0) + 10.0);
    
    // interpolation based on iq's blog article on gradient noise
    // https://www.iquilezles.org/www/articles/gradientnoise/gradientnoise.htm
    return a0 + 
           u.x *(b0 - a0) + 
           u.y *(c0 - a0) + 
           u.z *(a1 - a0) + 
           u.x * u.y * (a0 - b0 - c0 + d0) + 
           u.y * u.z * (a0 - c0 - a1 + c1) + 
           u.z * u.x * (a0 - b0 - a1 + b1) + 
           u.x * u.y * u.z * (-a0 + b0 + c0 - d0 + a1 - b1 - c1 + d1);
    
}


/// Multi-octave perlin noise compounder
float perlinFBM(vec3 p, float freq, int octaves) {
    float noise = 0.;
    float w = 0.5;
    float c = 1.;
    for (int i = 0; i < octaves; ++i) {
        noise += w * perlin(p , freq * c);
        c = c * 2.;
        w *= 0.5;
    }
    return noise;
}

/// Puffy Perlin
// had to do some witchcraft here to look like figure 4.5 in GPU Pro 7
// tbh not sure why it was like this
float puffyPerlin(vec3 p, float freq) {
    float puff = abs(abs(perlinFBM(p, freq, 7)) * 2. - 1.);
    return remap(puff, 1., 0., 0., 1.);
}

//// PerlinWorley

/// Perlin-Worley noise combined
// puffy Perlin noise is remapped with Worley as the new min
float perlin_worley(vec3 uv, float freq) {
    float w = worleyFBM(uv, freq);
    float p = puffyPerlin(uv, freq);
    return remap(p, 0., 1., w, 1.);
}

/// cloud
// a combination of Perlin-Worley noise and a few octaves of Worley noise
// described in GPU Pro 7 within the "Cloud Sampler" section
float cloud(vec3 uv, float freq) {
    float pw = perlin_worley(uv, freq);
    float wg = worleyFBM(uv, freq);
    float wb = worleyFBM(uv, freq * 2.);
    float wa = worleyFBM(uv, freq * 4.);
    
    float wfbm = wg * .625 + wb * .25 + wa * 0.125;
    
    return remap(pw, wfbm - 1., 1., 0., 1.);
}


// image will be comprised of t x t tiles
float t = 2.0; 
// noises start generation with freq x freq cells
float freq = 4.0;


void main (void) {
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = gl_FragCoord.xy/iResolution.xy;
    
    // create tiles
    uv *= t;
    // add time XY animation
    uv -= 0.05 * iTime;
    uv = fract(uv);
    
    float w = 1. - fract(2. * gl_FragCoord.z);

    vec3 uvw = vec3(uv, w);
    
    
    vec3 col = vec3(0.0);
  /// => comment/uncomment these lines to see individual parts
    //col += worley(uvw * freq, freq);
    //col += worleyFBM(uvw, freq);
    //col = vec3(perlin(uvw, freq));
    //col = vec3(perlinFBM(uvw, freq, 7));
    //col = vec3(puffyPerlin(uvw, freq));
    //col = vec3(perlin_worley(uvw, freq));
    
    //if (gl_FragCoord.x > iResolution.x / 2.) col = vec3(remap(cloud(uvw, freq), 0.69, 1., 0., 1.));
    //else col = vec3(cloud(uvw, freq));
    
    
    // Output color
    //gl_FragColor = vec4(col,1.0);
    gl_FragColor = vec4(remap(cloud(uvw, freq), 0.69, 1., 0., 1.));
}
