uniform vec2 iResolution;
uniform float iTime;


uniform vec4 bgCol;

uniform float dist;
uniform vec3 camera;
uniform vec3 origin_to_target;
uniform vec3 left;
uniform vec3 up;

uniform vec3 sun_dir;
uniform float lopac;

uniform vec3 coneVectors[8];


uniform sampler3D tex;


#define ray_it 90
#define light_it 8 // aka length of coneVectors
float inv_ray_it = 1. / float(ray_it);


float opacity = 65.;
vec3 cloudCol = vec3(0.3, 0.3, 0.7);

float light_opacity = 35.;
vec3 lightCol = vec3(.9, .1, .1);


// makes a sphere
// x - (length(p) * .05) is inverse distance function 
// points with length less than 20x get positive values, outside of x radius values become negative
// high enough values from the FBM can outweigh the negative distance values
float scene(vec3 p) {
    if ((1.5 - length(p) * .05) < 0.) return 0.;
    return .5 - (length(p) * .05) + texture3D(tex, p * 0.01).x;
}

// should be able to represent optical depth as length(p) * 0.05

void main( void ) {
    // Normalized pixel coordinates so that 0,0 is the center
    vec2 uv = (2. * gl_FragCoord.xy - iResolution.xy)/iResolution.y;

    /// raycasting 
    // rays start at origin
    vec3 origin = dist * camera.yxz;
    // normal from origin to pixel
    vec3 dir = normalize(uv.x * left + uv.y * up + 1.5 * origin_to_target);
    
    // ray loop things
    float zMax = sqrt(900. - (uv.x * uv.x + uv.y * uv.y) );
    float zTravel = 2. * zMax;
    float zStep = zTravel * inv_ray_it;
    vec3 rayPoint = origin;
    vec3 color = vec3(0.);
    float density = 0.;
    //Transmittence
    float T = 1.;

    // light loop things
    float lightSample;
    float lzMax = zTravel * 0.15;
    float lStep = lzMax * 0.125;
    vec3 light_step = sun_dir * lStep;
    float cone_spread_multiplier = length(light_step);
    
    // skip to start of cloud values
    rayPoint += dir * (2. * zMax - origin.z);


    float l_density = 0.;
    float LightEnergy = 0.;
    float accumulated_density = 0.;
    // ray loop
    for(int i = 0; i < ray_it; i ++) {
        density = scene(rayPoint);
        if (density > 0.) {
            
            T *= 1. - density; 
            density *= 0.01;
            accumulated_density += density;

            // allows transmittence to be "used up" as it goes through the cloud
            if (T <= .01) break;


            // light sampling
            // copy point for use in sampling loop
            vec3 samplePoint = rayPoint;
            for (int j = 1; j < 8; j++) {
                samplePoint += light_step
                    + (cone_spread_multiplier * float(j) * coneVectors[j]);
                
                lightSample = scene(samplePoint);
                if (lightSample > 0.) {
                    l_density += lightSample;
                }
            }

            // Beer's law states that
            // E = e^-t
            // where 
            // E is light engergy
            // t is thickness
            LightEnergy = exp(-1. * l_density);

            color += (cloudCol * opacity + lightCol * LightEnergy * light_opacity) * density * T;
            
        }
        rayPoint += dir * zStep;
    }

    // Output to screen
    gl_FragColor = vec4(color,1.0) + bgCol;
}
