#include "ProjectWindow.h"
#include <iostream>

// constructor / destructor
ProjectWindow::ProjectWindow(QWidget *parent) : QWidget(parent) {

	// create menu bar
	menuBar = new QMenuBar(this);

	//trying to make menu visible on mac
	menuBar->setNativeMenuBar(false);

	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);
	// link to close
	connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
	
	// add the item to the menu
	fileMenu->addAction(actionQuit);
	
	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	hLayout = new QBoxLayout(QBoxLayout::LeftToRight);
	subSliderLayout = new QBoxLayout(QBoxLayout::TopToBottom);
	sidebarLayout = new QGridLayout();


	// create main widget
	sceneWidget = new ProjectWidget(this);
	//windowLayout->addWidget(sceneWidget);
	subSliderLayout->addWidget(sceneWidget);

	// create timer
	nTimer = new QTimer;
	nTimer->start(10);
	connect(nTimer, SIGNAL(timeout()), sceneWidget, SLOT(updateTime()));

	// create horizontal view slider
	hSlider = new QSlider(Qt::Horizontal);
	hSlider->setMinimum(-300);
	hSlider->setMaximum(420);
	hSlider->setSliderPosition(60);
	connect(hSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateAngle(int)));

	// create sun slider
	sunSlider = new QSlider(Qt::Horizontal);
	sunSlider->setMinimum(-300);
	sunSlider->setMaximum(420);
	sunSlider->setSliderPosition(60);
	connect(sunSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateSun(int)));

	// create vertical view adjustment slider
	distSlider = new QSlider(Qt::Vertical);
	distSlider->setMinimum(0);
	distSlider->setMaximum(50);
	distSlider->setSliderPosition(0);
	connect(distSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateDist(int)));

	// create opacity adjustment slider
	coverageSlider = new QSlider(Qt::Vertical);
	coverageSlider->setMinimum(1);
	coverageSlider->setMaximum(80);
	coverageSlider->setSliderPosition(0);
	connect(coverageSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateCoverage(int)));

	// create light opacity adjustment slider
	lopacSlider = new QSlider(Qt::Vertical);
	lopacSlider->setMinimum(1);
	lopacSlider->setMaximum(100);
	lopacSlider->setSliderPosition(0);
	connect(lopacSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateLightOpacity(int)));

	// create cloud opacity adjustment slider
	opacSlider = new QSlider(Qt::Vertical);
	opacSlider->setMinimum(1);
	opacSlider->setMaximum(100);
	opacSlider->setSliderPosition(0);
	connect(opacSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateOpacity(int)));

	// create absorption adjustment slider
	absorptionSlider = new QSlider(Qt::Vertical);
	absorptionSlider->setMinimum(-300);
	absorptionSlider->setMaximum(-80);
	absorptionSlider->setSliderPosition(0);
	connect(absorptionSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateAbsorption(int)));

	// create frequency adjustment slider
	freqSlider = new QSlider(Qt::Vertical);
	freqSlider->setMinimum(50);
	freqSlider->setMaximum(200);
	freqSlider->setSliderPosition(0);
	connect(freqSlider, SIGNAL(valueChanged(int)), sceneWidget, SLOT(updateFreqmod(int)));

	

	sidebarLayout->addWidget(distSlider, 0, 0, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel("zoom"), 1, 0);
	sidebarLayout->addWidget(coverageSlider, 0, 1, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel("sparsity"), 1, 1);
	sidebarLayout->addWidget(lopacSlider, 0, 2, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel("  light\nintensity"), 1, 2, Qt::AlignHCenter);
	sidebarLayout->addWidget(opacSlider, 0, 3, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel(" cloud\nopacity"), 1, 3, Qt::AlignHCenter);
	sidebarLayout->addWidget(absorptionSlider, 0, 4, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel("  particle\nabsorption"), 1, 4, Qt::AlignHCenter);
	sidebarLayout->addWidget(freqSlider, 0, 5, Qt::AlignHCenter);
	sidebarLayout->addWidget(new QLabel(" octave\nsampled"), 1, 5, Qt::AlignHCenter);

	
	hLayout->addLayout(subSliderLayout, 10);
	hLayout->addLayout(sidebarLayout, 0);
	subSliderLayout->setStretchFactor(sceneWidget, 10);
	windowLayout->addLayout(hLayout, 0);
	subSliderLayout->addWidget(new QLabel("scene rotation"));
	subSliderLayout->addWidget(hSlider);
	subSliderLayout->addWidget(new QLabel("sun rotation"));
	subSliderLayout->addWidget(sunSlider);
	

} // constructor


ProjectWindow::~ProjectWindow()	{
	std::cout << "Average frame time was " << sceneWidget->getAvgFrameTime() << " milliseconds\n"; 
	std::cout << "Average  framerate was " << sceneWidget->getAvgFramerate() << " FPS\n"; 
	delete nTimer;
	delete lopacSlider;
	delete coverageSlider;
	delete hSlider;
	delete sceneWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
} // destructor

// resets all the interface elements
void ProjectWindow::ResetInterface() {
	
	// now force refresh
	sceneWidget->update();
	update();
} // ResetInterface()

