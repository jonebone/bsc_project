#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <vector>


class ProjectWidget: public QGLWidget	{

	Q_OBJECT

	public:
	ProjectWidget(QWidget *parent);

	double getAvgFrameTime();
	double getAvgFramerate();

	public slots:
        // called by the slider in the main window
	void updateAngle(int);
	void updateSun(int);
	void updateDist(int);
	void updateOpacity(int);
	void updateLightOpacity(int);
	void updateCoverage(int);
	void updateAbsorption(int);
	void updateFreqmod(int);
	void updateTime();

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();
	// noise related functions
	void makeConeNoiseVectors();
	void makeNoiseTexture(int resolution = 128, GLint format = GL_RED);

	private:
	  //models
	  void simpleCube();


	  //helper functions
	  void cross3x3(float*, float*, float*);
	  void normalize(GLfloat*);  
	  
	double _angle;
	double _sunAngle;
	double _distmod;
	double _time;
	double _opacity;
	double _light_opacity;
	double _clearsky;
	double _absorption;
	double _freqmod;
	float _dist;

	double _framerate;
	double _timedFrameCount;

	GLuint * _textures;
	GLuint _program;
	float *cloudRGB;
	float *sunRGB;
	
}; // class ProjectWidget
	
#endif
