#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QTimer>
#include <QPushButton>
#include <QLabel>
#include <QBoxLayout>
#include <QGridLayout>
#include "ProjectWidget.h"

class ProjectWindow: public QWidget { 

	Q_OBJECT
	
	public:  
	
	// constructor / destructor
	ProjectWindow(QWidget *parent);
	~ProjectWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;
	QBoxLayout *hLayout;
	QBoxLayout *subSliderLayout;
	QGridLayout *sidebarLayout;

	// beneath that, the main widget
	ProjectWidget *sceneWidget;
	// add sliders
	QSlider *hSlider;
	QSlider *distSlider;
	QSlider *sunSlider;
	QSlider *coverageSlider;
	QSlider *opacSlider;
	QSlider *lopacSlider;
	QSlider *absorptionSlider;
	QSlider *freqSlider;
	// add timer for animation
	QTimer *nTimer;

	// resets all the interface elements
	void ResetInterface();

}; 
	
#endif
