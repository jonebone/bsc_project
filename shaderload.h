// to run on mac: use OpenGl/glu.h and GLUT/glut.h
#ifdef __APPLE__
#include <OpenGl/glu.h>
#include <GLUT/glut.h>
#else
// to run on Dec10: use GL/glu.h and GL/glut.h
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <iostream>
#include <fstream>

#include <stdlib.h>

GLuint loadFragShader(const char * path, bool safe = true) {
    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);

    std::ifstream shader_file (path, std::ifstream::in);

    if (!shader_file.is_open()) {
        std::cerr << "File " << path << " failed to open \n";
        return 0;
    }
    // seek to end of file
    shader_file.seekg(0,shader_file.end);
    // report int position of end of file
    int fileLength = shader_file.tellg();
    // seek back to start
    shader_file.seekg(0, shader_file.beg);

    //read file to char array
    char * shaderSource = new char[fileLength + 1];
    shader_file.read(shaderSource, fileLength);
    shader_file.close();

    // ensure last char is null terminator
    shaderSource[fileLength] = '\0';

    // debug things
    GLint success = GL_FALSE;
    int logLength;

    // compiling shader
    glShaderSource(fragShader, 1, &shaderSource, NULL);
    glCompileShader(fragShader);
    
    //check frag shader
    if (safe) {
        glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
            char fragShaderErrorLog[logLength];
            glGetShaderInfoLog(fragShader, logLength, NULL, &fragShaderErrorLog[0]);
            printf("Error compiling %s \n%s \n", path, fragShaderErrorLog);
            exit(EXIT_FAILURE);
        }
    }

    // linking program
    GLuint program = glCreateProgram();
    glAttachShader(program, fragShader);
    glLinkProgram(program);

    // check program linking
    if (safe) {
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (!success) {
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
            char programErrorLog[logLength];
            glGetProgramInfoLog(program, logLength, NULL, &programErrorLog[0]);
            printf("Error in program \n%s\n", programErrorLog);
            exit(EXIT_FAILURE);
        }
    }

    // shader exists as part of program, do not need to store it elsewhere
    glDeleteShader(fragShader);

    return program;
}
