// NB: for linux use path of models/filename or textures/filename or shaders/filename
//     for mac use just filename
// to run on mac: use OpenGl/glu.h and GLUT/glut.h
#ifdef __APPLE__
#include <OpenGl/glu.h>
#include <GLUT/glut.h>
#define FRAG_SHADER "finalFragShader.frag"
#else
// to run on Dec10: use GL/glu.h and GL/glut.h
#include <GL/glu.h>
#include <GL/glut.h>
#define FRAG_SHADER "shaders/finalFragshader.frag"
#endif

#include <QGLWidget>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string.h>

#include <chrono>

#include "ProjectWidget.h"
#include "shaderload.h"
#include "noisemaker.h"
#include "texturemaker.h"

#define PI 3.14159265


// constructor //
ProjectWidget::ProjectWidget(QWidget *parent)
  : QGLWidget(parent),
    _angle(60),
    _sunAngle(60),
    _distmod(0),
    _time(0),
    _opacity(0),
    _light_opacity(0),
    _clearsky(0),
    _absorption(0),
    _freqmod(0),
    _framerate(0),
    _timedFrameCount(0)
	{ // constructor
  cloudRGB = new float[3];
  sunRGB   = new float[3];
  cloudRGB[0] = 0.6, cloudRGB[1] = 0.6, cloudRGB[2] = 0.6;
  sunRGB[0]   = 0.8, sunRGB[1]   = 0.5, sunRGB[2]   = 0.1;         

} // constructor

double ProjectWidget::getAvgFrameTime() {
  return _framerate/(_timedFrameCount * 1000);
}

double ProjectWidget::getAvgFramerate() {
  return 1000000 / (_framerate / _timedFrameCount);
}

void ProjectWidget::makeConeNoiseVectors() {
  GLfloat vectors[24];
  float temp_vec[3];
  cone_sample_vector(_time + 84, 173, temp_vec);
  vectors[0] = temp_vec[0], vectors[1] = temp_vec[1], vectors[2] = temp_vec[2];
  cone_sample_vector(_time + 36, 182, temp_vec);
  vectors[3] = temp_vec[0], vectors[4] = temp_vec[1], vectors[5] = temp_vec[2];
  cone_sample_vector(_time + 5, 63, temp_vec);
  vectors[6] = temp_vec[0], vectors[7] = temp_vec[1], vectors[8] = temp_vec[2];
  cone_sample_vector(_time + 49, 223, temp_vec);
  vectors[9] = temp_vec[0], vectors[10] = temp_vec[1], vectors[11] = temp_vec[2];
  cone_sample_vector(_time + 90, 199, temp_vec);
  vectors[12] = temp_vec[0], vectors[13] = temp_vec[1], vectors[14] = temp_vec[2];
  cone_sample_vector(_time + 27, 204, temp_vec);
  vectors[15] = temp_vec[0], vectors[16] = temp_vec[1], vectors[17] = temp_vec[2];
  cone_sample_vector(_time + 90, 199, temp_vec);
  vectors[18] = temp_vec[0], vectors[19] = temp_vec[1], vectors[20] = temp_vec[2];
  cone_sample_vector(_time + 27, 204, temp_vec);
  vectors[21] = temp_vec[0], vectors[22] = temp_vec[1], vectors[23] = temp_vec[2];

  GLint varLocation = glGetUniformLocation(_program, "coneVectors");
  if (varLocation != -1) glUniform3fv(varLocation, 8, vectors);
} // makeConeNoiseVectors()

void ProjectWidget::makeNoiseTexture(int resolution, GLint format) {
  int dataSize = resolution;
  int format_multiplier = 1;
  if (format == GL_RGBA) format_multiplier = 4;
  printf("Loading noise texture... \n");

  auto start = std::chrono::high_resolution_clock::now();

  float * data = new float[dataSize * dataSize * dataSize * format_multiplier];

  for(int i = 0; i < dataSize; i ++) {
    for(int j = 0; j < dataSize; j ++) {
      for (int k = 0; k < dataSize; k ++) {
        if (format_multiplier == 4) {
          data[(dataSize * (dataSize * i + j) + k) * 4    ] = perlin_worley_fbm((float)k, (float)j, (float)i );
          data[(dataSize * (dataSize * i + j) + k) * 4 + 1] = worley_fbm((float)k, (float)j, (float)i ,  9);
          data[(dataSize * (dataSize * i + j) + k) * 4 + 2] = worley_fbm((float)k, (float)j, (float)i , 27);
          data[(dataSize * (dataSize * i + j) + k) * 4 + 3] = worley_fbm((float)k, (float)j, (float)i , 49);
        } else {
          data[dataSize * (dataSize * i + j) + k] = cloud_fbm((float)k, (float)j, (float)i);
        }
      }
    }
  }
  _textures[0] = tex3D(data, dataSize, format);

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

  printf(" texture loaded - time taken %lld milliseconds\n", duration.count());
} // makeNoiseTexture()


// called when OpenGL context is set up
void ProjectWidget::initializeGL()	{ // initializeGL()
  // print OpenGL version
  printf("%s\n", glGetString(GL_VERSION));
	// set the widget background colour
  float bgCol[4] = {0.3, 0.3, 0.3, 0.};
	glClearColor(bgCol[0], bgCol[1], bgCol[2], 1.);
  
	glOrtho(-4.,4.,-1.,7.,-4.,50);

  _textures = new GLuint[3];
  int toTest = 64;
  makeNoiseTexture(toTest);

  _program = loadFragShader(FRAG_SHADER);
  glUseProgram(_program);

  _dist = 75.0f - (_distmod * 0.2f);

  GLint varLocation = glGetUniformLocation(_program, "bgCol");
  if (varLocation != -1) glUniform4fv(varLocation, 1, bgCol);
  varLocation = glGetUniformLocation(_program, "cloudColor");
  if (varLocation != -1) glUniform4fv(varLocation, 1, cloudRGB);
  varLocation = glGetUniformLocation(_program, "sunColor");
  if (varLocation != -1) glUniform4fv(varLocation, 1, sunRGB);
  varLocation = glGetUniformLocation(_program, "dist");
  if (varLocation != -1) glUniform1f(varLocation, _dist);

  makeConeNoiseVectors();
} // initializeGL()

void ProjectWidget::updateAngle(int i) {
  _angle = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time  / 100.0);
  this->repaint();
} // updateAngle(int i)

void ProjectWidget::updateSun(int i) {
  _sunAngle = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time  / 100.0);
  this->repaint();
} //updateSun(int i)

void ProjectWidget::updateDist(int i) {
  _distmod = i;
  _dist = 75.0f - (_distmod * 0.2f);
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "dist");
  if (varLocation != -1) glUniform1f(varLocation, _dist);
  this->repaint();
} // updateDist(int i)

void ProjectWidget::updateOpacity(int i) {
  _opacity = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "opac");
  if (varLocation != -1) glUniform1f(varLocation, (float)_opacity);
  this->repaint();
} // updateOpacity(int i)

void ProjectWidget::updateLightOpacity(int i) {
  _light_opacity = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "lopac");
  if (varLocation != -1) glUniform1f(varLocation, (float)_light_opacity);
  this->repaint();
} // updateLightOpacity(int i)

void ProjectWidget::updateCoverage(int i) {
  _clearsky = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "clearsky");
  if (varLocation != -1) glUniform1f(varLocation, (float)_clearsky / 100.0);
  this->repaint();
} // updateCoverage(int i)

void ProjectWidget::updateAbsorption(int i) {
  _absorption = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "absorb");
  if (varLocation != -1) glUniform1f(varLocation, (float)_absorption * -0.01);
  this->repaint();
} // updateAbsorbtion(int i)

void ProjectWidget::updateFreqmod(int i) {
  _freqmod = i;
  _time += 1; // slots cannot be simultaneously triggered so this allows time to keep moving while adjusting
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  varLocation = glGetUniformLocation(_program, "freqmod");
  if (varLocation != -1) glUniform1f(varLocation, (float)_freqmod / 100.0);
  this->repaint();
} // updateFreqmod(int i)

void ProjectWidget::updateTime() {
  _time += 1 ;
  GLint varLocation = glGetUniformLocation(_program, "iTime");
  if (varLocation != -1) glUniform1f(varLocation, (float) _time / 100.0);
  if ((int)_time % 10 == 0) {
    auto start = std::chrono::steady_clock::now();
    this->repaint();
    auto stop = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    _framerate += duration.count();
    _timedFrameCount += 1;
  } else this->repaint();
} // updateTime()

// fun! :)
// called every time the widget is resized
void ProjectWidget::resizeGL(int w, int h) { // resizeGL()
  //std::cout << "width " << w << ", height " << h << "\n";
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

  // update the uniform resolution variable in the frag shader
  GLint varLocation = glGetUniformLocation(_program, "iResolution");
  if (varLocation != -1) {
    float res[2] = {(float) w, (float) h};
    glUniform2fv(varLocation, 1, res);
  }

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
  
  float aspectRatio = (float) w / (float) h;
  if (w >= h) glOrtho(-4.0 * aspectRatio, 4.0 * aspectRatio, -1.0, 7.0, -4.0, 24.0);
  else glOrtho(-4.0, 4.0, -1.0 / aspectRatio, 7.0 / aspectRatio, -4.0, 24.0);
} // resizeGL()


void ProjectWidget::normalize(GLfloat* vector) {
  GLfloat norm = sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);
  vector[0] /= norm;
  vector[1] /= norm;
  vector[2] /= norm; 
} // normalize(GLfloat* vector)


void ProjectWidget::simpleCube() {
  glPushMatrix();
    glTranslatef(0., 0., 1.9);
    glutSolidCube(5.50);
  glPopMatrix();
} // simpleCube()


void ProjectWidget::cross3x3(float * a, float * b, float* out) {
  out[0] = a[1] * b[2] - a[2] * b[1];
  out[1] = a[2] * b[0] - a[0] * b[2];
  out[2] = a[0] * b[1] - a[1] * b[0];
} // cross3x3(float* a, float* b, float* c)


// called every time the widget needs painting
void ProjectWidget::paintGL() { // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_NORMALIZE);
      	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
        // 3.141/180 = 0.01745
        float pos[3] = {8.0f * sinf((float)_angle *  0.01745), 8.0f * cosf((float)_angle *  0.01745), 3.55};
        float target[3] = {0., 0., -1.};
        float up[3] = {0., 0., 1.}; 

        gluLookAt(pos[0], pos[1], pos[2],   
                  target[0], target[1], target[2],   
                  up[0], up[1], up[2]);


        // raymarching vectors
        normalize(pos);
        float origin_to_target[3] = {target[0] - _dist * pos[1], target[1] - _dist * pos[0], target[2] - _dist * pos[2]};
        normalize(origin_to_target);
        float left[3];
        cross3x3(up, origin_to_target, left);
        normalize(left);
        cross3x3(origin_to_target, left, up);
        normalize(up);


        GLfloat sun_dir[] = {5.0f * cosf((float)_sunAngle * 0.01745), 5.0f * sinf((float)_sunAngle * 0.01745), -2.7f, 0.0f};
        glLightfv(GL_LIGHT0, GL_POSITION, sun_dir);
        float sun[3] = {sun_dir[0], sun_dir[1], sun_dir[2]};
        normalize(sun);

        GLint varLocation = glGetUniformLocation(_program, "camera");
        if (varLocation != -1) glUniform3fv(varLocation, 1, pos);
        varLocation = glGetUniformLocation(_program, "origin_to_target");
        if (varLocation != -1) glUniform3fv(varLocation, 1, origin_to_target);
        varLocation = glGetUniformLocation(_program, "left");
        if (varLocation != -1) glUniform3fv(varLocation, 1, left);
        varLocation = glGetUniformLocation(_program, "up");
        if (varLocation != -1) glUniform3fv(varLocation, 1, up);
        varLocation = glGetUniformLocation(_program, "sun_dir");
        if (varLocation != -1) glUniform3fv(varLocation, 1, sun);
    


  //insert model
  this->simpleCube();
  
	// flush to screen
	glFlush();	
} // paintGL()
